# Turtlebot client repository

Raspberry pie running ubuntu mate.

For installation and documentation see:
http://emanual.robotis.com/docs/en/platform/turtlebot3/overview/#overview

ssh to client from remote:
```
turtlepi@xxx.xxx.xx.xx
password: a
```